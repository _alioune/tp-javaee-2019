package org.alioune.tpjavaee.rest;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;

import org.alioune.tpjavaee.ejb.CommuneEJB;
import org.alioune.tpjavaee.model.Commune;

@Path("commune")
public class CommuneRS {
	
	@EJB
	private CommuneEJB communeEJB;
	
	/*@GET @Path("{id}")
	public String getCommune(@PathParam("id") long id) {
		return "Requete pour la commune "+id;
	}*/
	@GET @Path("{id}")
	@Produces(MediaType.APPLICATION_XML)
	public Commune getCommune(@PathParam("id") long id) {
		Commune commune=new Commune(id,"Paris");
		return commune;
	}
	@POST @Path("create")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response create(@FormParam("name") String name) {
		long id=communeEJB.createCommune(name);
		return Response.ok()
				.entity("Commune cr��e avec l'id"+id+"/"+name).build();
	}

}
