package org.alioune.tpjavaee.rest;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.alioune.tpjavaee.ejb.MaireEJB;
import org.alioune.tpjavaee.model.Commune;
import org.alioune.tpjavaee.model.Maire;

public class MaireRS {
	@EJB
	private MaireEJB maireEJB;
	/*@GET @Path("{id}")
	public Maire getMaire(@PathParam("id") long id) {
		return maireEJB.getMaireById(id);
	}*/
	@GET @Path("{id}")
	@Produces(MediaType.APPLICATION_XML)
	public Maire getMaire(@PathParam("id") long id) {
		Maire maire=new Maire(id,"BEYE","Alioune");
		return maire;
	}
	@POST @Path("create")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response create(@FormParam("nom") String nom,@FormParam("prenom") String prenom) {
		long id=maireEJB.createMaire(nom,prenom);
		return Response.ok()
				.entity("Commune cr��e avec l'id"+id+"/"+nom+"/"+prenom).build();
	}
	
	

}
