package org.alioune.tpjavaee.ejb;
import javax.ejb.Stateless;
import javax.persistence.*;

import org.alioune.tpjavaee.model.Maire;
@Stateless
public class MaireEJB {
	@PersistenceContext(unitName="tp-rest")
	private EntityManager em;
	public long createMaire(String nom,String prenom) {
		Maire maire = new Maire();
		maire.setNom(nom);
		maire.setPrenom(prenom);
		em.persist(maire);
		return maire.getId();
	}
	public Maire getMaireById(long id) {
		return (Maire)em.find(Maire.class, id);
	}

}
