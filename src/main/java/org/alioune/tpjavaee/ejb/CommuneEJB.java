package org.alioune.tpjavaee.ejb;

import javax.ejb.Stateless;
import javax.persistence.*;

import org.alioune.tpjavaee.model.Commune;
@Stateless
public class CommuneEJB {
	
	@PersistenceContext(unitName="tp-rest")
	private EntityManager em;
	
	
	public long createCommune(String name) {
		Commune commune = new Commune();
		commune.setNom(name);
		em.persist(commune);
		return commune.getId();
	}
	
	
}
